
function toglerFunction() {

    const toggleButton = document.getElementById('switch');

    const anualColor = document.querySelector(".annual-title");
    const monthlyColor = document.querySelector(".monthly-title");

    const basicPrice =document.querySelector('.basic-price');
    const proPrice =document.querySelector('.pro-price');
    const masterPrice =document.querySelector('.master-price');

    if(toggleButton.checked){
        basicPrice.innerHTML = 19.99;
        proPrice.innerHTML = 24.99;
        masterPrice.innerHTML = 39.99;
        anualColor.style.color = 'hsl(233, 13%, 49%)';
        monthlyColor.style.color = 'hsl(236, 72%, 79%)';
        
    }
    else{
        basicPrice.innerHTML = 199.99;
        proPrice.innerHTML = 249.99;
        masterPrice.innerHTML = 399.99;
        anualColor.style.color = 'hsl(236, 72%, 79%)';
        monthlyColor.style.color = 'hsl(233, 13%, 49%)';
    }
    
}